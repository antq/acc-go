// 8 queens.
package main

import (
	"fmt"
	"strconv"
	"time"
)

const DEBUG = false

type Board struct {
	size   int
	queens []int
	board  [][]int
}

// initialize board - min (SUPPORTED) size is 1, max size is 9
func (b *Board) Init(SIZE int) {
	b.size = SIZE
	b.queens = make([]int, b.size)
	b.board = make([][]int, b.size)

	for i := 0; i < b.size; i++ {
		var s = make([]int, b.size)
		b.board[i] = s
	}
}

func (b *Board) set_square_empty(row, col int) {
	b.board[row][col] = 0
}
func (b *Board) set_square_queen(row, col int) {
	b.board[row][col] = 1
}
func (b *Board) clear_possible_solution() {
	for i := 0; i < b.size; i++ {
		b.set_square_empty(b.queens[i], i)
	}
	b.queens = make([]int, b.size)
}

func (b *Board) set_possible_solution(queen_position_for_each_file []int) {
	for i := 0; i < b.size; i++ {
		b.queens[i] = queen_position_for_each_file[i]
	}
	for i := 0; i < b.size; i++ {
		b.set_square_queen(b.queens[i], i)
	}
}

func (b *Board) str() string {
	r := "    "

	for x := 0; x < b.size; x++ {
		r += "_" + string(rune(int('a')+x)) + "_ "
	}
	r += "\n"
	for y := b.size - 1; y > -1; y-- {
		r += " " + strconv.Itoa(y+1) + " |"
		for x := 0; x < b.size; x++ {
			var c string
			if b.square_not_empty(y, x) {
				c = "♛"
			} else {
				c = "_"
			}
			r += "_" + c + "_|"
		}
		r += " " + strconv.Itoa(y+1) + "\n"
	}
	r += "    "
	for x := 0; x < b.size; x++ {
		r += " " + string(rune(int('a')+x)) + "  "
	}
	r += "\n"
	return r
}

func (b *Board) square_not_empty(row, col int) bool {
	if b.board[row][col] == 0 {
		return false
	}
	if b.board[row][col] == 1 {
		return true
	}
	panic(fmt.Sprintf("board[%d][%d]==%d\n", row, col, b.board[row][col]))
}

func (b *Board) count_diagonal1(rank, file int) int {
	count := 0

	// position to upper left memory wise, which is lower left presentation wise
	i, j := rank, file

	for i > 0 && j > 0 {
		i, j = i-1, j-1
	}
	for i < b.size && j < b.size {
		if DEBUG {
			print(i, j, b.size)
		}
		if b.square_not_empty(i, j) {
			count += 1
		}
		i, j = i+1, j+1
	}

	return count
}

func (b *Board) count_diagonal2(rank, file int) int {
	count := 0

	// position to lower left memory wise, which is upper left presentation wise
	i, j := rank, file
	for i < (b.size-1) && j > 0 {
		i, j = i+1, j-1
	}

	for i >= 0 && j < b.size {
		if DEBUG {
			print(i, j, b.size)
		}
		if b.square_not_empty(i, j) {
			count += 1
		}
		i, j = i-1, j+1
	}

	return count
}

func (b *Board) fails_diagonal(rank, file int) bool {
	if DEBUG {
		print('\t', rank, file, b.size)
	}
	diagonal_1 := b.count_diagonal1(rank, file)
	diagonal_2 := b.count_diagonal2(rank, file)

	if diagonal_1 == 1 && diagonal_2 == 1 {
		return false
	}
	return true
}

func (b *Board) count_row(rank int) int {
	count := 0
	for j := 0; j < b.size; j++ {
		if b.square_not_empty(rank, j) {
			count += 1
		}
	}
	return count
}

// returns False if only 1 nonempty square in rank (row)
// self.rank must be set (0..self.size-1) prior to calling
// main tests against q[m]!=q[n]; violation is exceptional
func (b *Board) fails_row(rank int) bool {
	count := b.count_row(rank)
	if count != 1 {
		fmt.Println(b.str())
		panic("count(rank " + strconv.Itoa(rank) + ") == " + strconv.Itoa(count))

	}
	return false
}

func (b *Board) count_col(file int) int {
	count := 0
	for i := 0; i < b.size; i++ {
		if b.square_not_empty(i, file) {
			count += 1
		}
	}
	return count
}

// returns False if only 1 nonempty square in file (column)
// self.file must be set (0..self.size-1) prior to calling
// one queen per element defined; violation is exceptional
func (b *Board) fails_col(file int) bool {
	count := b.count_col(file)
	if count != 1 {
		fmt.Println(b.str())
		panic("count(file " + strconv.Itoa(file) + ") == " + strconv.Itoa(count))
	}
	return false
}

func (b *Board) fails_solution() bool {
	// By definition, each board is defined as having one Queen in each file (column),
	// through cont bypass, each board should only have one Queen in each rank (row),
	// Row nor col should fail, but do row first as elements in row are memory aligned.
	// Ideally, after testing, we would only test the diagonals.
	for i := 0; i < b.size; i++ {
		if b.fails_row(i) {
			return true
		}
	}
	for q := 0; q < b.size; q++ {
		if b.fails_diagonal(b.queens[q], q) {
			return true
		}
	}
	for j := 0; j < b.size; j++ {
		if b.fails_col(j) {
			return true
		}
	}
	return false
}

func main() {
	start := time.Now()
	countSolution, countSkipped, countAttempted := 0, 0, 0

	size := 8
	var b = &Board{size, make([]int, 0), make([][]int, 0)}
	b.Init(size)
	for r1 := 0; r1 < size; r1++ {
		for r2 := 0; r2 < size; r2++ {
			if r1 == r2 {
				countSkipped++
				continue
			}
			for r3 := 0; r3 < size; r3++ {
				if r1 == r3 || r2 == r3 {
					countSkipped++
					continue
				}
				for r4 := 0; r4 < size; r4++ {
					if r1 == r4 || r2 == r4 || r3 == r4 {
						countSkipped++
						continue
					}
					for r5 := 0; r5 < size; r5++ {
						if r1 == r5 || r2 == r5 || r3 == r5 || r4 == r5 {
							countSkipped++
							continue
						}
						for r6 := 0; r6 < size; r6++ {
							if r1 == r6 || r2 == r6 || r3 == r6 || r4 == r6 || r5 == r6 {
								countSkipped++
								continue
							}
							for r7 := 0; r7 < size; r7++ {
								if r1 == r7 || r2 == r7 || r3 == r7 || r4 == r7 || r5 == r7 || r6 == r7 {
									countSkipped++
									continue
								}
								for r8 := 0; r8 < size; r8++ {
									if r1 == r8 || r2 == r8 || r3 == r8 || r4 == r8 || r5 == r8 || r6 == r8 || r7 == r8 {
										countSkipped++
										continue
									}
									countAttempted++
									if DEBUG {
										print(r1, r2, r3, r4, r5, r6, r7, r8)
									}
									q := []int{r1, r2, r3, r4, r5, r6, r7, r8}
									// atomicity: begins
									b.set_possible_solution(q)
									if b.fails_solution() {
										b.clear_possible_solution()
										// atomicity: ends
										continue
									}
									countSolution += 1
									fmt.Println(b.str())
									b.clear_possible_solution()
								}
							}
						}
					}
				}
			}
		}
	}
	fmt.Printf("%d Solutions (%d Attempted, %d Skipped) %s\n",
		countSolution, countAttempted, countSkipped, time.Since(start))
}
