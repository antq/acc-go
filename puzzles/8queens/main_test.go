package main

import (
	"testing"
)

func TestInit(t *testing.T) {
	var b Board
	b.Init(8)
	l := len(b.board)
	if l != b.size {
		t.Errorf("row count = %d, want %d\n", l, b.size)
	}
	for i := 0; i < l; i++ {
		l = len(b.board[i])
		if l != b.size {
			t.Errorf("col[%d] count = %d, want %d\n", i, l, b.size)
		}
	}
}

func TestStr(t *testing.T) {
	var b Board
	var str, b_str string
	var q []int
	s5 := "    _a_ _b_ _c_ _d_ _e_ \n" +
		" 5 |_♛_|___|___|___|___| 5\n" +
		" 4 |___|_♛_|___|___|___| 4\n" +
		" 3 |___|___|_♛_|___|___| 3\n" +
		" 2 |___|___|___|_♛_|___| 2\n" +
		" 1 |___|___|___|___|_♛_| 1\n" +
		"     a   b   c   d   e  \n"
	s8 := "    _a_ _b_ _c_ _d_ _e_ _f_ _g_ _h_ \n" +
		" 8 |_♛_|___|___|___|___|___|___|___| 8\n" +
		" 7 |___|_♛_|___|___|___|___|___|___| 7\n" +
		" 6 |___|___|_♛_|___|___|___|___|___| 6\n" +
		" 5 |___|___|___|_♛_|___|___|___|___| 5\n" +
		" 4 |___|___|___|___|_♛_|___|___|___| 4\n" +
		" 3 |___|___|___|___|___|_♛_|___|___| 3\n" +
		" 2 |___|___|___|___|___|___|_♛_|___| 2\n" +
		" 1 |___|___|___|___|___|___|___|_♛_| 1\n" +
		"     a   b   c   d   e   f   g   h  \n"

	for _, size := range []int{5, 8} {
		b.Init(size)
		q = make([]int, size)
		for i := 0; i < size; i++ {
			q[i] = b.size - i - 1
		}
		b.set_possible_solution(q)
		b_str = b.str()
		switch size {
		case 5:
			str = s5
		case 8:
			str = s8
		}
		if b_str != str {
			t.Errorf("str():\n%s\nwant:\n%s\n", b_str, str)
			t.Errorf("str():\n%q\nwant:\n%q\n", b_str, str)
		}
	}
}

// Confirms row, column, and diagonals count up correctly. Leverages
// observation for size N, there are N squares in row, N squares in
// column, and N squares diagonally related for any square on border
func TestCount(t *testing.T) {
	var b Board
	var q []int
	var r, c, d int

	for size := 1; size < 1025; size++ {
		b.Init(size)
		q = make([]int, size)
		for i := 0; i < size; i++ {
			for j := 0; j < size; j++ {
				q[j] = i
			}
			b.set_possible_solution(q)
		}
		for _, i := range []int{0, b.size - 1} {
			r = b.count_row(i)
			if r != size {
				t.Errorf("row count: %d\twant: %d\n", r, size)
			}
			c = b.count_col(i)
			if c != size {
				t.Errorf("col count: %d\twant: %d\n", c, size)
			}
			for _, j := range []int{0, b.size - 1} {
				cd1, cd2 := b.count_diagonal1, b.count_diagonal2
				d = cd1(i, j) + cd2(i, j) - 1
				if d != size {
					t.Errorf("dia count: %d (%d+%d-1)\twant: %d \n",
						d, cd1(i, j), cd2(i, j), size)
				}
			}
		}
	}

}
